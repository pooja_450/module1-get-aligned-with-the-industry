# <b>CNN SUMMARY</b>

## <b>Need For CNN</b>
why doesn’t a simple fully connected neural network work for Image Processing? <br/>
    - Because of ‘Parameter Explosion’<br/>
    - Consider a simple 100×100 pixel image. And let’s say, we have 20 hidden layers with 50 neurons in each layer. So for training the network, 
      the total number of parameters in this fully connected neural network to process 100×100 pixel image would be 100x100x50x20 + bias which is
       more than 10000000 parameters.This is rightly known as ‘Parameter Explosion’.<br/>
    - CNN works with a lesser number of parameters and hence also less prone to overfitting! 

## <b>What is CNN ?</b>
   - Convolutional neural networks are composed of multiple layers of artificial neurons.
   - The behavior of each neuron is defined by its weights. When fed with the pixel values, the artificial neurons of a CNN pick out various  
     visual features.
   - When you input an image into a ConvNet, each of its layers generates several activation maps. Activation maps highlight the relevant 
     features of the image.     
   - Each of the neurons takes a patch of pixels as input, multiplies their color values by its weights, sums them up, and runs them through the
     activation function.  

<img src="https://i1.wp.com/bdtechtalks.com/wp-content/uploads/2019/08/Artificial-Neuron.png?resize=884%2C420&ssl=1" height=200 width=400><br/>

<b>The first (or bottom) layer of the CNN usually detects basic features such as horizontal, vertical, and diagonal edges. The output of the first layer is fed as input of the next layer, which extracts more complex features, such as corners and combinations of edges. As you move deeper into the convolutional neural network, the layers start detecting higher-level features such as objects, faces, and more.</b>

<img src="https://i1.wp.com/bdtechtalks.com/wp-content/uploads/2019/02/neural-networks-deep-learning-artificial-intelligence.png?resize=1024%2C797&ssl=1" height=500 width=700><br/>

## <b>Components of CNN</b>
<br/>
<h3><b>1. convolutional Layers:</b></h3>
Convolutional Layers are formed by applying the sliding window function to the matrix representing the image. This sliding window function is known as kernel or filter which is nothing but another matrix. In the below example, a 3 x 3 kernel is slid across the 10 x 10 input matrix. The 3 x 3 kernel matrix (K) is multiplied with every 3 x 3 matrix available from the input matrix (I).<br/>
<img src="https://i0.wp.com/cdn-images-1.medium.com/max/800/1*vjWdmQOxV-TgpaATrQ14xg.png?w=1080&ssl=1">
<img src="https://glassboxmedicine.files.wordpress.com/2019/07/convgif.gif?w=526&zoom=2"><br/>

<h3><b>2. Pooling Layers</b></h3>
The rectified output of the convolution layer is then passed to the pooling layer. Pooling is used to reduce the size of the input matrix to the subsequent layer. Max pooling is the most commonly used pooling method.

The feature map is scanned in 2 x 2 fashion and the maximum value within the four-pixel value is chosen. 2 x 2 is the most common size chosen for pooling.<br/>
<img src="https://i1.wp.com/cdn-images-1.medium.com/max/800/1*sv0iYe9Tve6dA8YvpYEGuQ.png?w=1080&ssl=1">

Pooling is done to extract the most important feature from the feature map. Other pooling techniques are average pooling, min pooling, etc. The most common one used in CNN is max pooling.

<h3><b>3. DROPOUT</b></h3>
Like the feed-forward network, dropout is used in CNN. Dropdown is used after pooling layers to avoid overfitting.<br/>

<h3><b>4. FULLY CONNECTED LAYERS</b></h3>

CNN can contain multiple convolution and pooling layers. Finally, the output of the last pooling layer of the network is flattened and is given to the fully connected layer. The number of hidden layers and the number of neurons in each hidden layer are the parameters that needed to be defined. Sigmoid and Softmax activation functions are used at these layers to output the class probability.

<img src="https://i1.wp.com/cdn-images-1.medium.com/max/800/1*k0XxkqIPKxTXmOjid9QSgw.png?w=1080&ssl=1">


