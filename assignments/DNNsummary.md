# ***DEEP NEURAL NETWORKS***

## **Neural Networks :**
- A neural network mimics a neuron, which has dendrites, a nucleus, axon, and terminal axon.
- A neural network is a network or circuit of neurons, or in a modern sense, an artificial neural network, composed of artificial neurons or nodes
-  Artificial Neural networks are a network of interconnected artificial neurons (or nodes) where each neuron represents an information processing unit. These interconnected nodes pass information to each other mimicking the human brain. 

<img src="https://nickmccullum.com/images/python-deep-learning/understanding-neurons-deep-learning/neuron-functionality.png" height=400px width=700px>

## **Model of an Artificial Neuron**

<img src="https://www.tutorialspoint.com/python_deep_learning/images/probable_model.jpg" height=400px width=700px>

- The circles are neurons or nodes, with their functions on the data and the lines/edges connecting them are the weights/information being passed along.
- Each column is a layer. The first layer of your data is the input layer. Then, all the layers between the input layer and the output layer are the hidden layers.
- If you have one or a few hidden layers, then you have a ***shallow neural network***.
- If you have many hidden layers, then you have a ***deep neural network***.

## **Feed Forward Neural Networks :**
- The act of sending data straight through a neural network is called a feed forward neural network.
- In a typical "feed forward", the most basic type of neural network, you have your information pass straight through the network you created, and you compare the output to what you hoped the output would have been using your sample data.

## **Non-Feed Forward Neural Networks (Back Propogation) :**
- When we go backwards and begin adjusting weights to minimize loss/cost, this is called back propagation.
This is an optimization problem.
<br>

## **Understanding the working of Neural Network :**

<img src="https://nickmccullum.com/images/python-deep-learning/understanding-neurons-deep-learning/activation-function.png" height=400px width=700px>

- when a neuron receives its inputs from the neurons in the preceding layer of the model, it adds up each signal multiplied by its corresponding weight and passes them on to an activation function

***X=(weight * input)+bais***

- After this, an activation funtion is applied then fed to the next layer

***Y=Activation( ∑(Weights * Inputs)+ bais )***

## ***Types of Activation Functions :***

- **Threshold Functions**

Threshold functions compute a different output signal depending on whether or not its input lies above or below a certain threshold. Remember, the input value to an activation function is the weighted sum of the input values from the preceding layer in the neural network.

<img src="https://nickmccullum.com/images/python-deep-learning/deep-learning-activation-functions/threshold-function.png" height=400px width=500px>

- **Sigmoid Function**

The sigmoid function is well-known among the data science community because of its use in logistic regression, one of the core machine learning techniques used to solve classification problems.

<img src="https://nickmccullum.com/images/python-deep-learning/deep-learning-activation-functions/sigmoid-function.png" height=400px width=500px>

- **Rectifier Function**

The rectifier function does not have the same smoothness property as the sigmoid function from the last section. However, it is still very popular in the field of deep learning.

The rectifier function is defined as follows:
    
- If the input value is less than 0, then the function outputs 0
- If not, the function outputs its input value

<img src="https://nickmccullum.com/images/python-deep-learning/deep-learning-activation-functions/rectifier-function.png" height=400px width=500px>

- **Hyperbolic Tangent Function**

The hyperbolic tangent function is the only activation function included in this tutorial that is based on a trigonometric identity

<img src="https://nickmccullum.com/images/python-deep-learning/deep-learning-activation-functions/hyperbolic-tangent-function.png" height=400px width=500px>

## ***Cost Function:***

Cost functions in machine learning are functions that help to determine the offset of predictions made by a machine learning model with respect to actual results during the training phase.

## ***Gradient Descent:***

Gradient is simply a vector which gives the direction of maximum rate of change. By taking steps in that direction, we hope to reach our optimal solution.